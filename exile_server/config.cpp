////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 5.48
//'now' is Sat May 06 11:12:29 2017 : 'file' last modified on Sat May 06 11:12:29 2017
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

//Class D:\BitBucket\Exilebackup\exile_server\config.bin{
class CfgPatches
{
	class exile_server
	{
		requiredVersion = 0.1;
		requiredAddons[] = {"exile_client","exile_assets","exile_server_config"};
		units[] = {};
		weapons[] = {};
		magazines[] = {};
		ammo[] = {};
	};
};
class CfgFunctions
{
	class ExileServer
	{
		class Bootstrap
		{
			file = "exile_server\bootstrap";
			class preInit
			{
				preInit = 1;
			};
			class postInit
			{
				postInit = 1;
			};
		};
		class FiniteStateMachine
		{
			file = "exile_server\fsm";
			class main
			{
				ext = ".fsm";
			};
		};
	};
};
class cfgMods
{
	author = "";
	timepacked = "1494054749";
};
//};
